function saveOptions(e){
	e.preventDefault();
	if (document.querySelector("#ropt1").checked){ radval=0 } else {radval=1};
	browser.storage.local.set({
		ext_settings: {updnotif: document.querySelector("#set3").checked,
		updnotif_type: radval},

		tweak_options: {hidefr_n: document.querySelector("#set1").checked, 
		hidemsg_n: document.querySelector("#set2").checked,
		hidetr_n: document.querySelector("#set2_1").checked,
		adblock: document.querySelector("#adblock").checked,
		hide_chatbox: document.querySelector("#hide_chatbox").checked
	}

		/*Old structure
		hidefr_n: document.querySelector("#set1").checked, 
		hidemsg_n: document.querySelector("#set2").checked,
		hidetr_n: document.querySelector("#set2_1").checked,
		updnotif: document.querySelector("#set3").checked,
		adblock: document.querySelector("#adblock").checked, 
		updnotif_type: radval,*/
	});
	browser.runtime.sendMessage("update adblock");
	if (migrate_proc){
		browser.storage.local.remove(["migration_stat","hidefr_n","hidemsg_n","hidetr_n","adblock","updnotif","updnotif_type"])
		document.querySelector("#pt1").style.display = "none"
		document.querySelector("#errornote").style.display = "none"
		document.querySelector("#instruction").style.display = "none"
	}
	if (saveindi.innerHTML == "Saved."){
			saveindi.style.opacity = 0.5;
			setTimeout(function(){saveindi.style.opacity = 1}, 500);
			clearTimeout(inditm)
	}
	saveindi.style.display = "block"
	saveindi.innerHTML = "Saved."
	inditm = setTimeout(function(){saveindi.style.display = "none";saveindi.innerHTML = "Save indicator"},2520)
}

var radval = "", inditm, errnotif, saveindi, migrate_proc
	
function restoreOptions(){
		saveindi = document.getElementById("saveindicator");
		document.querySelector("form").addEventListener("submit", saveOptions);
		function setCurrentChoice(result){
			document.querySelector("#set1").checked = result.tweak_options.hidefr_n || result.hidefr_n;
			document.querySelector("#set2").checked = result.tweak_options.hidemsg_n || result.hidemsg_n;
			document.querySelector("#set2_1").checked = result.tweak_options.hidetr_n || result.hidetr_n;
			document.querySelector("#set3").checked = result.ext_settings.updnotif || result.updnotif;
			document.querySelector("#adblock").checked = result.tweak_options.adblock || result.adblock;
			document.querySelector("#hide_chatbox").checked = result.tweak_options.hide_chatbox
			radval =  result.ext_settings.updnotif_type || result.updnotif_type;
			if (radval==0){
				document.querySelector("#ropt1").checked=true 
			} else {
				document.querySelector("#ropt2").checked=true
			}
			if (typeof result.migration_stat !== "undefined"){ // Check if the "migration_stat" variable exists
			migrate_proc=true
				if (result.migration_stat == "success"){
					document.querySelector("#pt1").style.display = "block"
					document.querySelector("#instruction").style.display = "block"
				} else if (result.migration_stat == "error") {
					document.querySelector("#pt1").style.display = "block"
					document.querySelector("#errornote").style.display = "block"
					document.querySelector("#instruction").style.display = "block"
				}
			}
			if (result.tswitch==false){
				document.querySelector("#toggle_warning").style.display = "block"
			}
			radiobutton_gate()
}
		function onError(error){
			//alert('Error: ${error}');
			errnotif = browser.notifications.create({
			"type":"basic",
			"title":"FireBlox - Error",
			"message":`Uh Oh: FireBlox was unable to save your settings! If this error keeps appearing, please send this information to the creator:\n${error}`
			})
		}
		
		var getting = browser.storage.local.get();
		getting.then(setCurrentChoice, onError);
		
}
function radiobutton_gate(){ // State of radio button depends on checkbox.
	var ncheckbox = document.getElementById("set3");
	var radio1=document.getElementById("ropt1");
	var radio2=document.getElementById("ropt2");
	if (ncheckbox.checked){
		   radio1.disabled=false
		   radio2.disabled=false
	   }
	ncheckbox.onchange = function(){
	   if (ncheckbox.checked){
		   radio1.disabled=false
		   radio2.disabled=false
	   } else {
		   radio1.disabled=true
		   radio2.disabled=true
	   }}
};
window.addEventListener("DOMContentLoaded", restoreOptions)//;
//window.addEventListener("DOMContentLoaded", radiobutton_gate)