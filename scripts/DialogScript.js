var locked = false, overlay
var dialogs={
	createoverlay: function(){
		if (!locked){ // Temporary measure.
			overlay = document.createElement("div");
			overlay.className = "overlay";
			locked=true
			return overlay;
		} else {
			return true
		}
	},
	create: function(HeaderText, ctype, content){
		var dframe = document.createElement("div");
		dframe.className = "dialog-content";
		var dheader = document.createElement("div");
		dheader.className = "dialog-header";
		var hobj = document.createElement("h2");
		var htext = document.createTextNode(HeaderText);
		/*var mbody = document.createElement("div");
		mbody.className = "dialog-body";*/
		var closeb = document.createElement("span");
		closeb.className="close";
		closeb.innerHTML = "&times;"
		closeb.onclick = function(){
			overlay.style.opacity = "0";
			setTimeout(function(){overlay.parentNode.removeChild(overlay)}, 500);
			locked=false
		}
		
		var mcontent;
		if (ctype=="embed"){
			mcontent = document.createElement("iframe");
			mcontent.style.width = "100%";
			mcontent.style.height = "500px";
			mcontent.style.border = "0px"
			mcontent.src = content} else if (ctype=="normal"){
				mcontent = document.createTextNode(content);
		};
		//.createTextNode(content)
		//--- Adding the elements --
		hobj.appendChild(closeb);
		dframe.appendChild(dheader);
		dframe.appendChild(mcontent);
		dheader.appendChild(hobj);
		hobj.appendChild(htext);
		return dframe
	}/*, For when this script is more prepare in the future.
	nudge: function(_element){
		
	},
	close_overlay: function(overlay){
		locked=false;
		document.removeChild(overlay);
	}*/
	}