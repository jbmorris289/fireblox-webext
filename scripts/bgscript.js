function onInstall(data){
	//console.log(data.reason) //For testing.
	if (data.reason == "install"){
		var instDate = new Date();
		browser.storage.local.set({installtd: instDate.toString(), tswitch: true, tweak_options: Object(), ext_settings: Object()});
		browser.storage.local.get().then(function(res){console.log(res.tweak_options)});

		var stpg = browser.runtime.getURL("pages/FirstRun_config.html");
		browser.tabs.create({
			url: stpg
		})
	} else if (data.reason == "update"){
		browser.storage.local.get().then(function(res){
			// browser.storage.local.get().then(function(res){console.log(res)}) For testing
			// console.log((typeof res.ext_settings || typeof res.tweak_options || typeof res.migration_stat) == "undefined")
			if ((typeof res.ext_settings || typeof res.tweak_options || typeof res.migration_stat) == "undefined"){
				console.log("updated");
				console.log("Migrating settings")
				let new_data_structure=migrate_storage_data(res)
				let move_migration = browser.storage.local.set({tweak_options: new_data_structure.tweak_options, ext_settings: new_data_structure.ext_settings}) // Check Promise for Errors
				move_migration.then(function(){
					browser.storage.local.set({migration_stat: "success"})
				},function(){
					browser.storage.local.set({migration_stat: "error"})
				})
				/*let migrate_note=browser.notifications.create({
					"type":"basic",
					"title":"FireBlox - Extension Notification",
					"message":"A change to the structure of setting storage was applied, and your settings (for this extension) were migrated to use the structure. To complete this process, please check your settings."
				})*/
		}
			if ((res.updnotif || res.ext_settings.updnotif)==true){
				if ((res.updnotif_type || res.ext_settings.updnotif_type)==0){
					browser.tabs.create({
						url: browser.runtime.getURL("pages/UpdatePage.html")
				})} else { 
					var update_msg = browser.notifications.create({
						"type":"basic",
						"title":"FireBlox - Update Notification",
						"message":"FireBlox has been updated! See the changes: https://firebloxext.blogspot.com, or the extension page on AMO.\nClick this notification to dismiss (or it will be dismissed automatically)."
					})
			}}
			if (res.countershow){
				browser.storage.local.remove("countershow")}
		})	
	}
}

function delete_extension_urls(histItem){
	openingpgs.forEach(function(item){
		if (histItem.url = browser.extension.getURL("pages/" + item)){
			browser.history.deleteUrl({url: histItem.url}) 
		}
	})
}

var adblock_value=false
var openingpgs = ["FirstRun_config.html","optionspage.html","UpdatePage.html"];

function set_adblock_value(res){
	//console.log(res)
	if ((res.tweak_options.adblock || res.adblock) && res.tswitch){
		adblock_value = true
	} else {
		adblock_value = false
}
}
var move_to={
	"tweak_options": ["hidefr_n","hidemsg_n","hidetr_n","adblock"],
	"ext_settings": ["updnotif","updnotif_type"]
}
function migrate_storage_data(r){
	var newdata={}
	
    for (category in move_to){
		newdata[category]={}
      for (var prop in move_to[category]){
		  if (typeof r[move_to[category][prop]] !== undefined){
			  newdata[category][move_to[category][prop]] = r[move_to[category][prop]]
		  }
}}
//console.log
return newdata
}
var req_pattern = ["*://adservice.google.com/adsid/integrator.js?domain=www.roblox.com", "*://securepubads.g.doubleclick.net/*","*://www.roblox.com/user-sponsorship/*"];
function blockreq(details){
	if (adblock_value){
		return {cancel: true}
}};

function onruntimemsg(msg){
	//console.log(msg) // For testing.
	if (msg=="update adblock")
		browser.storage.local.get(["tweak_options","tswitch"]).then(set_adblock_value)
};

browser.runtime.onInstalled.addListener(onInstall);
browser.history.onVisited.addListener(delete_extension_urls);
browser.runtime.onMessage.addListener(onruntimemsg);
browser.storage.local.get(["adblock","tweak_options","tswitch"]).then(set_adblock_value);
browser.webRequest.onBeforeRequest.addListener(
	blockreq, {urls: req_pattern},
	["blocking"])