# Fireblox-webext
This is the enitre source of this extension, straight from my 
computer.<br />
Feel free to audit the source of FireBlox.<br />

To test conveniently, you'll need the following tools:
<ul>
<li><a href="https://nodejs.org/en/">Nodejs</a></li>
<li>Node Package Manager (npm) - installed with node</li>
<li>[Recommended, but optional] <a href="https://www.npmjs.com/package/web-ext">web-ext</a></li>
</ul>
If you are NOT using web-ext with your testing, follow the instructions on this page:<br />
<a href="https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/">Temporary installation in Firefox - Mozilla Extension Workshop</a>
